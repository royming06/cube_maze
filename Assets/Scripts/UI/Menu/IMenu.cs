﻿namespace UI
{
    public interface IMenu
    {
        void Show();
        void Hide();
        void Init();
        MenuType GetMenuType { get; }
    }
}