﻿using System;
using Game;
using Game.Time;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LoseMenu : MonoBehaviour, IMenu
    {
        [SerializeField]
        private TextMeshProUGUI _timeText;

        [SerializeField]
        private Button _replayLevelButton;

        [SerializeField]
        private Button _replayAllGameButton;

        private LevelService _levelService;

        private TimeService _timeService;


        public void Init()
        {
            _levelService = GameContext.Instance.LevelService;
            _timeService = GameContext.Instance.TimeService;
            _replayLevelButton.onClick.AddListener(OnReplayLevelButtonClick);
            _replayAllGameButton.onClick.AddListener(OnReplayAllGameButtonClick);  
        }

        private void OnReplayAllGameButtonClick()
        {
            _levelService.StartNewGame();
        }

        private void OnReplayLevelButtonClick()
        {
            _levelService.ReplayCurrentLevel();
        }

        public void Show()
        {
            _timeText.text = $"Your time is {_timeService.GetTime()}";
            gameObject.SetActive(true);
        }


        public void Hide()
        {
            _timeText.text = string.Empty;
            gameObject.SetActive(false);
        }

        public MenuType GetMenuType => MenuType.LoseMenu;
    }
}