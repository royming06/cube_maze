﻿using Game;
using TMPro;
using UnityEngine;

namespace UI
{
    public class GameMenu : MonoBehaviour, IMenu
    {
        private const float TimeUpdateText = 0.5f;

        [SerializeField]
        private TextMeshProUGUI _timerTextMeshProUGUI;

        private float _deltaTimeBeforeUpdateText;

        private void Update()
        {
            if (TimeUpdateText < _deltaTimeBeforeUpdateText)
            {
                _deltaTimeBeforeUpdateText = 0;
                _timerTextMeshProUGUI.text = GameContext.Instance.TimeService.GetTime().ToString();
            }

            _deltaTimeBeforeUpdateText += Time.deltaTime;
        }

        public void Show()
        {
            _timerTextMeshProUGUI.text = string.Empty;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            _timerTextMeshProUGUI.text = string.Empty;
            gameObject.SetActive(false);
        }

        public void Init()
        {
            
        }

        public MenuType GetMenuType => MenuType.MainMenu;
    }
}