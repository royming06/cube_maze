﻿using Game;
using Game.Time;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class WinMenu : MonoBehaviour, IMenu
    {
        [SerializeField]
        private TextMeshProUGUI _timeText;

        [SerializeField]
        private Button _replayCurrentLevelButton;

        [SerializeField]
        private Button _nextLevelButton;

        [SerializeField]
        private Button _startNewGameButton;

        [SerializeField]
        private GameObject _lastLevelText;

        private TimeService _timeService;
        private LevelService _levelService;


        public void Init()
        {
            _timeService = GameContext.Instance.TimeService;
            _levelService = GameContext.Instance.LevelService;
            _replayCurrentLevelButton.onClick.AddListener(OnReplayCurrentLevelClick);
            _nextLevelButton.onClick.AddListener(OnNextLevelButtonClick); 
            _startNewGameButton.onClick.AddListener(OnStartNewGameButtonClick);
        }

        private void OnStartNewGameButtonClick()
        {
            _levelService.StartNewGame();
        }

        private void OnNextLevelButtonClick()
        {
            _levelService.StartNextLevel();
        }

        private void OnReplayCurrentLevelClick()
        {
            _levelService.ReplayCurrentLevel();
        }

        public void Show()
        {
            gameObject.SetActive(true);
            _timeText.text = $"Your time is {_timeService.GetTime()}";
            bool lastLevel = _levelService.IsLastLevel();
            _lastLevelText.SetActive(lastLevel);
            _startNewGameButton.gameObject.SetActive(lastLevel);
            _nextLevelButton.gameObject.SetActive(!lastLevel);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            _timeText.text = string.Empty;
        }

        public MenuType GetMenuType => MenuType.WinMenu;
    }
}