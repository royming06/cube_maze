﻿using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Abstraction;
using Game.Event;
using UnityEngine;

namespace UI
{
    public class UIService : MonoBehaviour, IConfigurable
    {
        private readonly List<IMenu> _menus = new List<IMenu>();

        [SerializeField]
        private List<GameObject> _menusObjects;

        private EventSystem _eventSystem;


        public void Configure()
        {
            _eventSystem = GameContext.Instance.EventSystem;
            CacheMenu();
            HideAll();
            SubscribeOnEvents();
        }

        private void SubscribeOnEvents()
        {
            _eventSystem.LevelEvent.OnLevelLose += OnLevelLose;
            _eventSystem.LevelEvent.OnLevelWin += OnLevelWin;
            _eventSystem.LevelEvent.OnLevelStarted += OnLevelStarted;
        }

        private void OnLevelStarted()
        {
            HideAll();
            IMenu menu = GetMenu(MenuType.MainMenu);
            menu.Show();
        }

        private void OnLevelWin()
        {
            HideAll();
            IMenu menu = GetMenu(MenuType.WinMenu);
            menu.Show();
        }

        private void OnLevelLose()
        {
            HideAll();
            IMenu menu = GetMenu(MenuType.LoseMenu);
            menu.Show();
        }

        private void CacheMenu()
        {
            foreach (GameObject menusObject in _menusObjects)
            {
                IMenu menu = menusObject.GetComponent<IMenu>();
                menu.Init();
                _menus.Add(menu);
            }
        }

        private void HideAll()
        {
            foreach (IMenu menu in _menus)
            {
                menu.Hide();
            }
        }

        private IMenu GetMenu(MenuType menuType)
        {
            return _menus.First(m => m.GetMenuType == menuType);
        }

        private void OnDestroy()
        {
            _eventSystem.LevelEvent.OnLevelLose -= OnLevelLose;
            _eventSystem.LevelEvent.OnLevelWin -= OnLevelWin;
            _eventSystem.LevelEvent.OnLevelStarted -= OnLevelStarted;
        }
    }
}