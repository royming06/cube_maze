﻿using System.Collections.Generic;
using Game.Abstraction;
using Game.Event;
using Game.Time;
using UI;
using UnityEngine;

namespace Game
{
    public class GameContext
    {
        private static GameContext context;

        public static GameContext Instance
        {
            get
            {
                if (context == null)
                {
                    context = new GameContext();
                    context.Configure();
                }

                return context;
            }
        }


        private readonly List<IConfigurable> _configurables;
        public EventSystem EventSystem { get; }
        public TimeService TimeService { get; }
        public LevelService LevelService { get; }
        private UIService UIService { get; }


        private void Configure()
        {
            foreach (IConfigurable configurable in _configurables)
            {
                configurable.Configure();
            }
        }

        private GameContext()
        {
            EventSystem = new EventSystem();
            TimeService = new TimeService(EventSystem);

            LevelService = GameObject.FindObjectOfType<LevelService>(); //FindObjectOfType Call ones in all game
            UIService = GameObject.FindObjectOfType<UIService>();

            _configurables = new List<IConfigurable>()
            {
                UIService
            };
        }
    }
}