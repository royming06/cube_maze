﻿using Game;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private void Awake()
    {
        GameContext.Instance.LevelService.StartNextLevel();
    }
}