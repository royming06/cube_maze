﻿namespace Game.Abstraction
{
    public interface IConfigurable
    {
        void Configure();
    }
}