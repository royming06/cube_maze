﻿using Game;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class Exit : MonoBehaviour
{
    [SerializeField] private Material _closedMaterial;
    [SerializeField] private Material _openMaterial;

    private MeshRenderer _renderer;
    private bool _keyPicked;

    private void Awake()
    {
        _renderer = GetComponent<MeshRenderer>();
        Close();
        GameContext.Instance.EventSystem.LevelEvent.OnKeyPicked += Open;
        
    }

    private void OnDestroy()
    {
        GameContext.Instance.EventSystem.LevelEvent.OnKeyPicked -= Open;
    }

    private void Open()
    {
        _renderer.sharedMaterial = _openMaterial;
        _keyPicked = true;
    }

    private void Close()
    {
        _renderer.sharedMaterial = _closedMaterial;
    }

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();
        if (player == null)
        {
            return;
        }

        if (!_keyPicked)
        {
            return;
        }
        
        GameContext.Instance.EventSystem.LevelEvent.WinLevel();
        GameContext.Instance.EventSystem.TimerEvent.StopTimer();
    }
}