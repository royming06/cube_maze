﻿using UnityEngine;

namespace Game.Level.Controller
{
    public interface ITeleportable
    {
        void TeleportTo(Vector3 teleportPoint);
    }
}