﻿using Game.Level.Controller;
using UnityEngine;

[RequireComponent(typeof(MovementController))]
public class Player : MonoBehaviour, ITeleportable //Now marked Component
{
    public void TeleportTo(Vector3 teleportPoint)
    {
        transform.position = new Vector3(teleportPoint.x, transform.position.y, teleportPoint.z);
    }
}