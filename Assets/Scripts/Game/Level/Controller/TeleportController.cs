﻿using System;
using UnityEngine;

namespace Game.Level.Controller
{
    public class TeleportController : MonoBehaviour
    {
        [SerializeField]
        private Transform _exitFromTeleportPosition;

        private void OnTriggerEnter(Collider collision)
        {
            ITeleportable teleportable = collision.transform.GetComponent<ITeleportable>();
            if (teleportable == null)
            {
                return;
            }
            teleportable.TeleportTo(_exitFromTeleportPosition.position);
        }
    }
}