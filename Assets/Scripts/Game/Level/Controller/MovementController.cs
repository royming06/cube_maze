﻿using Game;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] private LayerMask _obstacleMask;
    [SerializeField] private float _step;

    private bool _gameOver;
    private void Awake()
    {
        GameContext.Instance.EventSystem.LevelEvent.OnLevelLose += OnLevelLose;
    }

    private void OnLevelLose()
    {
        _gameOver = true;
    }

    private void OnDestroy()
    {
        GameContext.Instance.EventSystem.LevelEvent.OnLevelLose -= OnLevelLose;
    }

    private void Update()
    {
        if (_gameOver)
        {
            return;
        }
        ProcessKeyPressing();
    }

    private void ProcessKeyPressing()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            TryMove(Vector3.forward);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            TryMove(Vector3.back);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            TryMove(Vector3.right);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            TryMove(Vector3.left);
        }
    }

    private void TryMove(Vector3 direction)
    {
        Ray forwardRay = new Ray(transform.position, direction);

        if (Physics.Raycast(forwardRay, out RaycastHit hit, _step, _obstacleMask))
        {
            return;
        }

        transform.forward = direction;
        transform.Translate(direction * _step, Space.World);
    }
}
