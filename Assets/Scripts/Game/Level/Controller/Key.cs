﻿using Game;
using UnityEngine;

public class Key : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player _))
        {
            GameContext.Instance.EventSystem.LevelEvent.PickUpKey();
            Destroy(gameObject);
        }
    }
}
