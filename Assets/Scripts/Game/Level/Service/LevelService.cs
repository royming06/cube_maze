﻿using System.Collections.Generic;
using Game;
using UnityEngine;

public class LevelService : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _levelPrefabs;

    [SerializeField]
    private Transform _levelContainer;

    private int _currentLevelIndex = -1;
    private GameObject _currentLevel;
    

    public void StartNextLevel()
    {
        Destroy(_currentLevel);
        _currentLevelIndex++;
        if (_currentLevelIndex >= _levelPrefabs.Count)
        {
            Debug.LogWarning("You should check prefab count");
            return;
        }

        StartCurrentLevel();
    }

    private void StartCurrentLevel()
    {
        GameObject levelPrefab = _levelPrefabs[_currentLevelIndex];
        _currentLevel = Instantiate(levelPrefab, _levelContainer.transform);
        GameContext.Instance.EventSystem.LevelEvent.StartLevel();
        GameContext.Instance.EventSystem.TimerEvent.StartTimer();
    }

    public void ReplayCurrentLevel()
    {
        Destroy(_currentLevel);
        StartCurrentLevel();
    }

    public void StartNewGame()
    {
        _currentLevelIndex = -1;
        StartNextLevel();
    }

    public bool IsLastLevel()
    {
        return _currentLevelIndex + 1 >= _levelPrefabs.Count;
    }
}