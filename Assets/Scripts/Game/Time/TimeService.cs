﻿using System;
using Game.Event;
using UnityEngine;

namespace Game.Time
{
    public class TimeService : IDisposable
    {
        private readonly EventSystem _eventSystem;
        private float _startTime;
        
        public TimeService(EventSystem eventSystem)
        {
            _eventSystem = eventSystem;
            _eventSystem.TimerEvent.OnStartTimerEvent += OnTimerStarted;
            _eventSystem.TimerEvent.OnStopTimerEvent += OnStopTimer;
        }

        private void OnStopTimer()
        {
            _startTime = 0;
        }

        private void OnTimerStarted()
        {
            _startTime = UnityEngine.Time.realtimeSinceStartup;
        }

        public int GetTime()
        {
            return Mathf.RoundToInt(UnityEngine.Time.realtimeSinceStartup - _startTime);
        }

        public void Dispose()
        {
            _eventSystem.TimerEvent.OnStartTimerEvent -= OnTimerStarted;
            _eventSystem.TimerEvent.OnStopTimerEvent -= OnStopTimer;
        }
    }
}