﻿namespace Game.Event
{
    public class EventSystem
    {
        public TimerEvent TimerEvent { get; } = new TimerEvent();
        public LevelEvent LevelEvent { get; } = new LevelEvent();
    }
}