﻿using System;

namespace Game.Event
{
    public class LevelEvent
    {
        public event Action OnKeyPicked;
        public event Action OnLevelWin;
        public event Action OnLevelLose;
        public event Action OnLevelStarted;

        public void StartLevel()
        {
            OnLevelStarted?.Invoke();
        }
        public void WinLevel()
        {
            OnLevelWin?.Invoke();
        }

        public void LoseLevel()
        {
            OnLevelLose?.Invoke();
        }

        public void PickUpKey()
        {
            OnKeyPicked?.Invoke();
        }
    }
}