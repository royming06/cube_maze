﻿using System;

namespace Game.Event
{
    public class TimerEvent
    {
        public event Action OnStartTimerEvent;

        public event Action OnStopTimerEvent;

        public void StopTimer()
        {
            OnStopTimerEvent?.Invoke();
        }
        public void StartTimer()
        {
            OnStartTimerEvent?.Invoke();
        }
    }
}